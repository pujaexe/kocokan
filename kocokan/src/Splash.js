(function(global) {

    var Splash = global.Splash = function(game) {};

    Splash.prototype.preload = function() {
        var game = this.game;
        game.load.image('splashGame', 'assets/phaser.png');
        
    };

    Splash.prototype.create = function() {
        var game = this.game;

        var splashGame = game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'splashGame');
        splashGame.anchor.setTo(0.5, 0.5);
        
        splashGame.alpha = 0;
        var splashGameTween = game.add.tween(splashGame).to({
                alpha: 1
            }, 2000, Phaser.Easing.Linear.None, false, 0, 0, true);
        
        splashGameTween.onComplete.add(function() {
            game.state.start('menu');
        });

        splashGameTween.delay(500).start();
    };

}(this));
