var facebookName = "";

function checkLoginState() {

     console.log("TEST");
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);

    });
}

function onLogin(response) {
     if (response.status == "connected") {
          FB.api("/me?fields=first_name", function(data) {
               facebookName = data.first_name;
          });
     }
}

function shareScore(n){
     FB.ui({
          method: "feed",
          link: "https://apps.facebook.com/tangkap-bebek/",
          caption: "Ayo Mainkan Tangkap Bebek",
          name: "Level Tertinggi Saya di Level " + n + "!",
          description: "Level Tertinggi Saya di Level " + n + " . Bisakah kamu melewati Level saya? dan dapatkan hadiah langsungnya!",
          picture: "https://tangkap-bebek.firebaseapp.com/asset/og.png"
     }, function(response){});
}

window.fbAsyncInit = function() {
     FB.init({
          appId      : '153216298510147',
          xfbml      : true,
          version    : 'v2.8'
     });

     FB.getLoginStatus(function(response) {
          if (response.status == "connected") {
               onLogin(response);
          } else {
               FB.login(function(response) {
                    onLogin(response)
               }, {scope: "user_friends, email"});
          }
     });
      
};   


(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));