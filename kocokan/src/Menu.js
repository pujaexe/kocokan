(function(global) {

    var Menu = global.Menu = function(game) {};
    
    
    Menu.prototype.create = function() {
        
	   	this.sky = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'sky');
        
        
        this.startButton = this.game.add.button(this.game.width/2, this.game.height/2, 'buttonPlay', this.startClick, this);
        this.startButton.anchor.setTo(0.5,0.5);
        //var bgSound = this.add.audio('backSound');
        //bgSound.play('', 0, 1, true);
       
        

    };

    Menu.prototype.startClick= function() {
        
        
        this.game.state.start('game');
        

	};
    
    Menu.prototype.update = function() {

	 
        

	};
    


  
}(this));
