(function(global) {

    var Boot = global.Boot = function(game) {};

    Boot.prototype.preload = function() {
       this.load.image('preloaderBar', 'assets/loading-bar.png');
    };

    Boot.prototype.create = function() {
        
        this.input.maxPointers = 1;
        
		
		this.state.start('preload');
    };
   

}(this));

