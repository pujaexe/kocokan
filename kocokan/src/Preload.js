(function(global) {

    var Preload = global.Preload = function() {};

    Preload.prototype = {
        init: function(){
            this.stage.disableVisibilityChange = true;
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.scale.setScreenSize=true;
            

        },
        preload: function preload() {
            
            //this.load.bitmapFont('fontTelolet', 'assets/font.png', 'assets/font.fnt');
            this.load.atlas('atlas', 'assets/dadu.png', 'assets/dadu.json');
            this.load.image('bgne', 'assets/bgKocokan.png');
            this.load.spritesheet('udeg', 'assets/udegSprite.png', 146, 263);
            this.load.spritesheet('strongking', 'assets/strongking.png', 353, 360);
            this.load.spritesheet('chip', 'assets/chip.png',40,40);
            this.load.image('sliderArrow', 'assets/sliderarrow.png');
            this.load.image('tombol', 'assets/tombol.png');
            this.load.image('chipIcon', 'assets/chipIcon.png');
            this.load.image('lapakKocokan', 'assets/lapakKocokan.png');
            this.load.image('sky', 'assets/sky.png');
            this.load.image('kotakUang', 'assets/kotakUang.png');

            this.load.image('buttonPlay', 'assets/buttonPlay.png');
            this.load.spritesheet('tombolBet', 'assets/tombolBet.png', 65, 65);
         
            //this.load.audio('gassPoll', ['sound/gasspoll.ogg']);
            //this.load.spritesheet('bis', 'assets/bis.png', 491, 278, 3);
            //this.load.spritesheet('gambarOrang', 'assets/Orang.png', 100, 80);
        
            this.preloadBar = this.add.sprite((530-311)/2, (900-27)/2, 'preloaderBar');
            this.load.setPreloadSprite(this.preloadBar);
         
        },
        create: function() {
            this.state.start('splash');
        }
    };

}(this));
