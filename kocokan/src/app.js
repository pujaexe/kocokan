
(function(global) {
    var targetWidth = 1440; // the width of the game we want
    var targetHeight = 960; // the height of the game we want

    var deviceRatio = (window.innerWidth/window.innerHeight); //device aspect ratio
    //var deviceRatio = (targetWidth/targetHeight); 
    
    var newRatio = (targetHeight/targetWidth)*deviceRatio; //new ratio to fit the screen

    var newWidth = targetWidth*newRatio;
    
    var newHeight = targetHeight;

    var gameWidth = newWidth;
    var gameHeight = newHeight;
    
    
    
    var game = new Phaser.Game(gameWidth,gameHeight, Phaser.AUTO, 'body');    
    
    
    
    game.scaleRatio = window.devicePixelRatio/3;
    
    game.Score=0;
    
    game.state.add('preload', Preload);
    game.state.add('splash', Splash);
    game.state.add('menu', Menu);
    game.state.add('game', Game);
    game.state.add('boot', Boot);

 
    
    game.state.start('boot');
    
   
}(this));








