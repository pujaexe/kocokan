
(function(global) {
    
    var Game = global.Game = function(game) {};
    
    var counter = 0;
    var textTimer = 0;
    var textTotalUang =0;
    var gambarDadu1="";
    var gambarDadu2="";
    var gambarDadu3="";
    var gambarDadu4="";
    var daduBaru=false;
    var daduHilang=true;
    var player;
    var dadu1;
    var dadu2;
    var dadu3;
    var tombol;
    var groupTombol;
    var groupBet;
    var groupDadu;
    var gambarPilih=0;
    var gambarCip;
    var groupChip;
    var totalUang=3000000;
    var udegSprite;
    var tweenDadu;
    var tweenDadu2;
    var circleCounterMax = 0;
    var circleCounter = null;
    var circleCounterDisplay = null;
    var circleTimer = null;
    var textMenang;
    var textKalah;
    
    Game.prototype.create= function () {
        
        socket = io.connect('http://localhost:5001');
        this.game.stage.backgroundColor = '#124184';
        var background = this.game.add.sprite(0,0,'bgne'); 
        background.width = this.game.width;
        background.height = this.game.height;  

        this.lapak = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'lapakKocokan');
        this.lapak.anchor.setTo(0.5,0.5);

        udegSprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY - 320, 'udeg');
        udegSprite.anchor.setTo(0.5,0.5);

        udegSprite.animations.add('ngocok', [0, 1, 2, 3, 4, 5, 6, ,7 ,8 ,9], 5, true);
        udegSprite.animations.add('bukak', [10, 11, 12, 13], 5, true);

        var strongking = this.game.add.sprite(this.game.world.centerX-200, this.game.world.centerY - 300, 'strongking');
        strongking.anchor.setTo(0.5,0.5);
        strongking.animations.add('ngeblink');
        strongking.animations.play('ngeblink', 20, true);

        circleCounter = circleCounterMax;
        circleCounterDisplay = this.add.text(this.game.world.centerX+200, this.game.world.centerY - 300, '' + circleCounter, { fontSize:'50px', fill: '#ffffff' });
        circleCounterDisplay.anchor.setTo(0.5, 0.5);

        textMenang = this.game.add.text(this.game.world.centerX, this.game.world.centerY+300, '', { font: '50px Arial', fill: '#ffffff' });
        textMenang.anchor.setTo(0.5, 0.5);

        textKalah = this.game.add.text(this.game.world.centerX, this.game.world.centerY+400, '', { font: '50px Arial', fill: '#ffffff' });
        textKalah.anchor.setTo(0.5, 0.5);

        circleTimer = this.game.add.graphics(this.game.world.centerX+200, this.game.world.centerY - 300);

        this.game.add.sprite(20, 20, 'chipIcon');
        textTotalUang = this.game.add.text(90, 40, '3M', { fontSize:'32px', fill: "#ffffff", align: "center" });

        groupDadu = this.game.add.group();

        gambarDadu1 = this.game.add.sprite(0, 0, 'atlas');
        gambarDadu1.frameName = 'gambar1.png';
            
        gambarDadu2 = this.game.add.sprite(200, 0, 'atlas');
        gambarDadu2.frameName = 'gambar1.png';

        gambarDadu3 = this.game.add.sprite(400, 0, 'atlas');
        gambarDadu3.frameName = 'gambar1.png';

        groupDadu.add(gambarDadu1);
        groupDadu.add(gambarDadu2);
        groupDadu.add(gambarDadu3);

        groupDadu.visible=false;
        
        
        //nampilin tombol di lapak
        groupTombol = this.game.add.group();
        tombol = groupTombol.create(this.game.world.centerX-230, this.game.world.centerY-120, 'tombol');
        tombol.name=1;
        tombol = groupTombol.create(this.game.world.centerX-32, this.game.world.centerY-120, 'tombol');
        tombol.name=2;
        tombol = groupTombol.create(this.game.world.centerX+170, this.game.world.centerY-120, 'tombol');
        tombol.name=3;
        tombol = groupTombol.create(this.game.world.centerX-230, this.game.world.centerY+70, 'tombol');
        tombol.name=4;
        tombol = groupTombol.create(this.game.world.centerX-32, this.game.world.centerY+70, 'tombol');
        tombol.name=5;
        tombol = groupTombol.create(this.game.world.centerX+170, this.game.world.centerY+70, 'tombol');
        tombol.name=6;
        groupTombol.setAll('inputEnabled', true);
        groupTombol.callAll('events.onInputDown.add', 'events.onInputDown', this.tombolDown);

        groupChip = this.game.add.group();
        //calon slider :
        // var sliderArrow = this.game.add.sprite(128, 128, 'sliderArrow');
        // sliderArrow.inputEnabled = true;
        // sliderArrow.input.enableDrag();
        // sliderArrow.input.enableSnap(32, 32, true, true);
        // sliderArrow.input.allowHorizontalDrag = false;
       
        this.tombolBet();
        groupBet.visible=false;

        this.eventHandlerNya();

    };

    Game.prototype.eventHandlerNya= function() {

        socket.on('connect', this.onSocketConnected);

        socket.on('disconnect', this.onSocketDisconnect);

        socket.on('new player', this.onNewPlayer.bind(null, socket, this.game));

        socket.on('timer update', this.onTimerUpdate.bind(null, socket, this.game));

        socket.on('timer bagiuang', this.onTimerBagiUang.bind(null, socket, this.game));

        socket.on("buka dadu", this.onBukaDadu.bind(null, socket, this.game));

        socket.on("spawnUang", this.onSpawnUang.bind(null, socket, this.game));
        
        socket.on('reset game', this.onResetGame);

        socket.on('ambil ukupan', this.onAmbilUkupan);

        socket.on('ambil ciak', this.onAmbilCiak);

        
    };
    
    Game.prototype.tombolDown = function(item){

        gambarPilih = item.name;

        groupBet.visible=true;

    };

    Game.prototype.tombolBetDown = function(sprite){
        //mengurangi tabungan pemain
        //totalUang=totalUang-sprite.name;
        //textTotalUang.setText('Dompet: ' + totalUang);

        //kirim pasangan ke server dan posisi uang random, biar sama di seluruh klien random pos nya
        
        socket.emit('pasang bet', { gambar: gambarPilih, jumlahBet: sprite.name, posX:0, posY:0});
        

        groupBet.visible=false;

    };

    Game.prototype.tombolBet = function(item){
        groupBet = this.game.add.group();

        var kotakUang = this.game.add.sprite(this.game.world.centerX, this.game.height-(157/2), 'kotakUang');
        kotakUang.anchor.setTo(0.5,0.5);


        var bet5k = this.game.add.sprite(this.game.world.centerX-295, this.game.height-100, 'tombolBet', 0);
        bet5k.inputEnabled = true;
        bet5k.name=5000;
        bet5k.events.onInputDown.add(this.tombolBetDown, this);
        
        var bet10k = this.game.add.sprite(this.game.world.centerX-220, this.game.height-100, 'tombolBet', 1);
        bet10k.inputEnabled = true;
        bet10k.name=10000;
        bet10k.events.onInputDown.add(this.tombolBetDown, this);
        
        var bet25k = this.game.add.sprite(this.game.world.centerX-145, this.game.height-100, 'tombolBet', 2);
        bet25k.inputEnabled = true;
        bet25k.name=25000;
        bet25k.events.onInputDown.add(this.tombolBetDown, this);

        var bet50k = this.game.add.sprite(this.game.world.centerX-70, this.game.height-100, 'tombolBet', 3);
        bet50k.inputEnabled = true;
        bet50k.name=50000;
        bet50k.events.onInputDown.add(this.tombolBetDown, this);

        var bet100k = this.game.add.sprite(this.game.world.centerX+5, this.game.height-100, 'tombolBet', 4);
        bet100k.inputEnabled = true;
        bet100k.name=100000;
        bet100k.events.onInputDown.add(this.tombolBetDown, this);

        var bet250k = this.game.add.sprite(this.game.world.centerX+75, this.game.height-100, 'tombolBet', 5);
        bet250k.inputEnabled = true;
        bet250k.name=250000;
        bet250k.events.onInputDown.add(this.tombolBetDown, this);

        var bet500k = this.game.add.sprite(this.game.world.centerX+150, this.game.height-100, 'tombolBet', 6);
        bet500k.inputEnabled = true;
        bet500k.name=500000;
        bet500k.events.onInputDown.add(this.tombolBetDown, this);

        var bet1m = this.game.add.sprite(this.game.world.centerX+225, this.game.height-100, 'tombolBet', 7);
        bet1m.inputEnabled = true;
        bet1m.name=1000000;
        bet1m.events.onInputDown.add(this.tombolBetDown, this);

        groupBet.add(kotakUang);
        groupBet.add(bet5k);
        groupBet.add(bet10k);
        groupBet.add(bet25k);
        groupBet.add(bet50k);
        groupBet.add(bet100k);
        groupBet.add(bet250k);
        groupBet.add(bet500k);
        groupBet.add(bet1m);
    };

    Game.prototype.onSocketConnected= function() {
        
        console.log('konek ke server');

        // kirim data player baru ke server
        socket.emit('new player',{ totalUang: totalUang});
        

    };

    Game.prototype.onSocketDisconnect= function() {
        
        console.log('kluar dari server');

    };


    Game.prototype.onNewPlayer= function(socket,scope,data)  {
        
        switch (data.gambarPasang) {
            case 1:
                var randX = scope.rnd.realInRange(scope.world.centerX-250, scope.world.centerX-200);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 2:
                var randX = scope.rnd.realInRange(scope.world.centerX-2, scope.world.centerX-50);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 3:
                var randX = scope.rnd.realInRange(scope.world.centerX+130, scope.world.centerX+200);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 4:
                var randX = scope.rnd.realInRange(scope.world.centerX-200, scope.world.centerX-260);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 5:
                var randX = scope.rnd.realInRange(scope.world.centerX-2, scope.world.centerX-50);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 6:
                var randX = scope.rnd.realInRange(scope.world.centerX+140, scope.world.centerX+200);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
        } 

        function gambarBetPasangan(x,y,scope,besaranBet){
            var jumlahUntukIndex;
            switch(besaranBet){
                case 5000:
                    jumlahUntukIndex=0;
                    break;
                case 10000:
                    jumlahUntukIndex=1;
                    break;
                case 25000:
                    jumlahUntukIndex=2;
                    break;
                case 50000:
                    jumlahUntukIndex=3;
                    break;
                case 100000:
                    jumlahUntukIndex=4;
                    break;
                case 250000:
                    jumlahUntukIndex=5;
                    break;
                case 500000:
                    jumlahUntukIndex=6;
                    break;
                case 1000000:
                    jumlahUntukIndex=7;
            }
            gambarCip = scope.add.sprite(x,y, 'chip',jumlahUntukIndex);
            groupChip.add(gambarCip);
        }
        if (data.konUdeg=='ngocok'){

            udegSprite.animations.play('ngocok',10,false,false);

        }
        
      
    };

    Game.prototype.onTimerUpdate= function(socket,scope,data) {
        counter = data.x;
        

        if (circleCounterMax==0){
            circleCounterMax=counter+1;
        }

        circleCounter=counter;
        circleCounterDisplay.text = '' + circleCounter;
        
        
        circleTimer.lineStyle(8, 0xffd900);
        
        var sa = scope.math.degToRad(-90+(360/circleCounterMax)*(circleCounterMax-(circleCounter+1)));
        var ea = scope.math.degToRad(-90+(360/circleCounterMax)*(circleCounterMax-circleCounter));
        
        var arc = circleTimer.arc(0, 0, 50, sa, ea, false);
        arc.alpha = 0;
        arc.beginFill(0xFF3300);
        arc.endFill();
        
        var tween = scope.add.tween(arc).to({
            alpha: 1
        }, 250);
        tween.start();
        
    };

    Game.prototype.onTimerBagiUang= function(socket,scope,data) {
        counter = data.x;
        circleCounter=counter;
        circleCounterDisplay.text = '' + circleCounter;
    };

    Game.prototype.onBukaDadu= function(socket,scope,data) {
        //nampilin dadu yang kluar
        circleTimer.clear();
        circleCounterMax=0;


        gambarDadu1.frameName = 'gambar'+ data.x +'.png';
        gambarDadu2.frameName = 'gambar'+ data.y +'.png';
        gambarDadu3.frameName = 'gambar'+ data.z +'.png';
        

        udegSprite.animations.play('bukak',10,false,false);

        groupTombol.visible=false;
        groupBet.visible=false;

        groupDadu.x=udegSprite.x-5;
        groupDadu.y=udegSprite.y+100;
        groupDadu.scale.set(0.1);
        groupDadu.pivot.x = 275;
        groupDadu.pivot.y = 50;

        
        tweenDadu = scope.add.tween(groupDadu.scale).to( { x: 1, y: 1 }, 1000, Phaser.Easing.Elastic.Out, true,500);
        tweenDadu.onStart.add(()=>{
            groupDadu.visible=true;
        }, this);
        
    };

    Game.prototype.onAmbilUkupan= function(data) {
        
        console.log('ukup : '+data.totalUkupnya);
        totalUang=totalUang+data.totalUkupnya;

        var totalUangBaru=intToString(totalUang);

        textTotalUang.setText('' + totalUangBaru);
        textMenang.setText('Menang : '+ data.totalUkupnya);

        function intToString (value) {
            var suffixes = ["", "K", "M", "B","T"];
            var suffixNum = Math.floor((""+value).length/3);
            var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000,suffixNum)) : value).toPrecision(4));
            if (shortValue % 1 != 0)  shortNum = shortValue.toFixed(1);
            return shortValue+suffixes[suffixNum];
        }

    };

    Game.prototype.onAmbilCiak= function(data) {
        
        console.log('ciak : '+data.ciak);
        totalUang=totalUang-data.ciak;
        
        
        var totalUangBaru=intToString(totalUang);

        textTotalUang.setText('' + totalUangBaru);
        textKalah.setText('Kalah : '+ data.ciak)

        function intToString (value) {
            var suffixes = ["", "K", "M", "B","T"];
            var suffixNum = Math.floor((""+value).length/3);
            var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000,suffixNum)) : value).toPrecision(4));
            if (shortValue % 1 != 0)  shortNum = shortValue.toFixed(1);
            return shortValue+suffixes[suffixNum];
        }

    };

    Game.prototype.onSpawnUang= function(socket,scope,data) {
        //nampilin gambar uang sesuai yang di pilih pemain


        //gambarDadu4 = scope.add.sprite(40, 50, 'musuh');
        //console.log(data.gambar, data.jumlahBet);
        switch (data.gambar) {
            case 1:
                var randX = scope.rnd.realInRange(scope.world.centerX-250, scope.world.centerX-200);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 2:
                var randX = scope.rnd.realInRange(scope.world.centerX-2, scope.world.centerX-50);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 3:
                var randX = scope.rnd.realInRange(scope.world.centerX+130, scope.world.centerX+200);
                var randY = scope.rnd.realInRange(scope.world.centerY-100, scope.world.centerY-140);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 4:
                var randX = scope.rnd.realInRange(scope.world.centerX-200, scope.world.centerX-260);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 5:
                var randX = scope.rnd.realInRange(scope.world.centerX-2, scope.world.centerX-50);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
                break;
            case 6:
                var randX = scope.rnd.realInRange(scope.world.centerX+140, scope.world.centerX+200);
                var randY = scope.rnd.realInRange(scope.world.centerY+50, scope.world.centerY+100);
                gambarBetPasangan(randX,randY,scope,data.jumlahBet);
        } 

        function gambarBetPasangan(x,y,scope,besaranBet){
            var jumlahUntukIndex;
            switch(besaranBet){
                case 5000:
                    jumlahUntukIndex=0;
                    break;
                case 10000:
                    jumlahUntukIndex=1;
                    break;
                case 25000:
                    jumlahUntukIndex=2;
                    break;
                case 50000:
                    jumlahUntukIndex=3;
                    break;
                case 100000:
                    jumlahUntukIndex=4;
                    break;
                case 250000:
                    jumlahUntukIndex=5;
                    break;
                case 500000:
                    jumlahUntukIndex=6;
                    break;
                case 1000000:
                    jumlahUntukIndex=7;
            }
            gambarCip = scope.add.sprite(x,y, 'chip',jumlahUntukIndex);
            groupChip.add(gambarCip);
        }

        
    };

    Game.prototype.onResetGame= function(data) {

        groupDadu.visible=false;
        udegSprite.animations.play('ngocok',10,false,false);
        groupTombol.visible=true;
        textKalah.setText('');

        textMenang.setText('');        
        
        groupChip.forEach(function(item) {  

            item.kill();
        
        });
    };
    
   
    
    Game.prototype.update = function(){
		
	};


}(this));
 
 
 
 